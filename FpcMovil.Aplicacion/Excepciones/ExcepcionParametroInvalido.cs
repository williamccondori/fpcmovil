﻿using System;

namespace FpcMovil.Aplicacion.Excepciones
{
    public class ExcepcionParametroInvalido : Exception
    {
        public ExcepcionParametroInvalido(string nombreParametro)
            : base($"El parámetro ingresado es inválido: {nombreParametro}")
        {

        }
    }
}
