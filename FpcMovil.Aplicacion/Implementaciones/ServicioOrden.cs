﻿using FpcMovil.Aplicacion.Dtos;
using FpcMovil.Aplicacion.Excepciones;
using FpcMovil.Aplicacion.Servicios;
using FpcMovil.Dominio.Repositorios;
using System.Collections.Generic;
using System.Linq;

namespace FpcMovil.Aplicacion.Implementaciones
{
    internal class ServicioOrden : IServicioOrden
    {
        private readonly IRepositorioOrden _repositorioOrden;

        public ServicioOrden(IRepositorioOrden repositorioOrden)
        {
            _repositorioOrden = repositorioOrden;
        }

        public IEnumerable<DtoOrden> ObtenerTodosPedidos(string idCliente)
        {
            if (string.IsNullOrEmpty(idCliente))
            {
                throw new ExcepcionParametroInvalido(nameof(idCliente));
            }

            return _repositorioOrden.ObtenerTodosPedidos(idCliente)
                .Take(50000)
                .Select(x => new DtoOrden { Id = x.Id, FechaEnvio = x.FechaEnvio.ToShortDateString(), FechaPedido = x.FechaPedido.ToShortDateString(), FechaTransaccion = x.FechaTransaccion.ToShortDateString() });
        }
    }
}

