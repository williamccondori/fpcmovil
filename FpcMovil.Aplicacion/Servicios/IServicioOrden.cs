﻿using FpcMovil.Aplicacion.Dtos;
using System.Collections.Generic;

namespace FpcMovil.Aplicacion.Servicios
{
    public interface IServicioOrden
    {
        IEnumerable<DtoOrden> ObtenerTodosPedidos(string idCliente);
    }
}
