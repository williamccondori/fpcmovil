﻿namespace FpcMovil.Aplicacion.Dtos
{
    public class DtoOrden
    {
        public int Id { get; set; }
        public string FechaPedido { get; set; }
        public string FechaTransaccion { get; set; }
        public string FechaEnvio { get; set; }
    }
}
