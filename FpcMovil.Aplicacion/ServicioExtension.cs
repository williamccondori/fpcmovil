﻿using FpcMovil.Aplicacion.Implementaciones;
using FpcMovil.Aplicacion.Servicios;
using Microsoft.Extensions.DependencyInjection;

namespace FpcMovil.Aplicacion
{
    public static class ServicioExtension
    {
        public static void AgregarCapaAplicacion(this IServiceCollection servicios)
        {
            servicios.AddScoped<IServicioOrden, ServicioOrden>();
        }
    }
}