using FpcMovil.Aplicacion;
using FpcMovil.Infraestructura.EntityFramework;
using FpcMovil.Infraestructura.EntityFramework.Contextos;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace FpcMovil.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();
            services.AddSwaggerGen();


            string connectionString = Configuration.GetConnectionString("BaseDatos");
            services.AddDbContext<ContextoSaf>(opciones => opciones.UseSqlServer(connectionString));


            services.AgregarCapaInfraestructuraEntityFramework();
            services.AgregarCapaAplicacion();

            // Mas informaci�n: https://www.netmentor.es/entrada/health-checks-asp-net
            services.AddHealthChecks();

            services.Configure<OpcionConsul>(Configuration.GetSection("ServiceDiscovery"));
            services.AddConsul();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
                app.UseDeveloperExceptionPage();
            }

            //app.UseHttpsRedirection();
            app.UseConsul();


            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                // Configuraci�n adicional de AddHealthChecks.
                endpoints.MapHealthChecks("/health");
            });
        }
    }
}