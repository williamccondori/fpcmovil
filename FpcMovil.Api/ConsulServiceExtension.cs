﻿using Consul;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using System;

namespace FpcMovil.Api
{
    public class OpcionServicio
    {
        public string Nombre { get; set; }
        public string Esquema { get; set; }
        public string Ip { get; set; }
        public int Puerto { get; set; }
    }


    public class OpcionConsul
    {
        public string Ip { get; set; }
        public OpcionServicio Servicio { get; set; }
    }

    public static class ConsulServiceExtension
    {
        public static IServiceCollection AddConsul(this IServiceCollection services)
        {
            if (services == null)
            {
                throw new ArgumentNullException(nameof(services));
            }

            services.AddSingleton<IConsulClient>(p => new ConsulClient(o =>
            {
                var options = p.GetRequiredService<IOptions<OpcionConsul>>().Value;


                if (!string.IsNullOrEmpty(options.Ip))
                {
                    o.Address = new Uri(options.Ip);
                }
            }));

            return services;
        }

        public static IApplicationBuilder UseConsul(this IApplicationBuilder app)
        {

            var applicationLifetime = app.ApplicationServices.GetRequiredService<IHostApplicationLifetime>();
            var clienteConsul = app.ApplicationServices.GetRequiredService<IConsulClient>();
            var opcionesConsul = app.ApplicationServices.GetRequiredService<IOptions<OpcionConsul>>()?.Value;

            if (!string.IsNullOrEmpty(opcionesConsul.Ip))
            {
                var servicioConsul = opcionesConsul.Servicio;

                var serviceId = $"{servicioConsul.Nombre}@{servicioConsul.Ip}:{servicioConsul.Puerto}";

                var http = $"{servicioConsul.Esquema}://{servicioConsul.Ip}:{servicioConsul.Puerto}/health";

                var registro = new AgentServiceRegistration
                {
                    ID = serviceId,
                    Address = $"{servicioConsul.Ip}",
                    Port = servicioConsul.Puerto,
                    Name = servicioConsul.Nombre,

                    Check = new AgentCheckRegistration
                    {
                        HTTP = http,
                        Notes = $"Servicio {servicioConsul.Nombre} iniciado correctamente en el puerto {servicioConsul.Nombre}",
                        Interval = TimeSpan.FromSeconds(10)
                    }
                };

                clienteConsul.Agent.ServiceDeregister(registro.ID).ConfigureAwait(true);
                clienteConsul.Agent.ServiceRegister(registro).ConfigureAwait(true);

                applicationLifetime.ApplicationStopping.Register(() =>
                {
                    clienteConsul.Agent.ServiceDeregister(registro.ID).ConfigureAwait(true);
                });
            }

            return app;
        }
    }
}
