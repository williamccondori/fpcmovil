﻿using FpcMovil.Aplicacion.Dtos;
using FpcMovil.Aplicacion.Servicios;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;

namespace FpcMovil.Api.Controllers
{
    [ApiController]
    [Route("api/pedidos")]
    public class ControladorOrden : ControladorBase<ControladorOrden>
    {
        private readonly IServicioOrden _servicioOrden;

        public ControladorOrden(IServicioOrden servicioOrden, ILogger<ControladorOrden> bitacora)
            : base(bitacora)
        {
            _servicioOrden = servicioOrden;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<DtoOrden>))]
        public IActionResult ObtenerTodosPedidos(string idCliente)
        {
            return Ejecutar(() =>
            {
                return _servicioOrden.ObtenerTodosPedidos(idCliente);
            });
        }
    }
}
