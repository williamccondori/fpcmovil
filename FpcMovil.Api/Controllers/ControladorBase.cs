﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;

namespace FpcMovil.Api.Controllers
{
    public class ControladorBase<T> : ControllerBase where T : class
    {
        protected readonly ILogger<T> _bitacora;

        protected ControladorBase(ILogger<T> bitacora)
        {
            _bitacora = bitacora;
        }

        protected IActionResult Ejecutar<TRespuesta>(Func<TRespuesta> operacion)
        {
            try
            {
                _bitacora.LogTrace("Ejecutando consulta {operacion}", nameof(operacion));
                return Ok(operacion());
            }
            catch (Exception exception)
            {
                _bitacora.LogError(exception.Message);

                return BadRequest();
            }
        }
    }
}
