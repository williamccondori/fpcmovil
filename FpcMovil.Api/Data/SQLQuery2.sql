
DECLARE @cnt INT = 0;
WHILE @cnt < 100000
BEGIN
INSERT INTO [dbo].[Orders]
           ([CustomerID]
           ,[EmployeeID]
           ,[OrderDate]
           ,[RequiredDate]
           ,[ShippedDate]
           ,[ShipVia]
           ,[Freight]
           ,[ShipName]
           ,[ShipAddress]
           ,[ShipCity]
           ,[ShipRegion]
           ,[ShipPostalCode]
           ,[ShipCountry])
     VALUES
           ('WELLI'
           ,3
           ,'1996-07-15 00:00:00.000'
           ,'1996-08-12 00:00:00.000'
           ,'1996-08-12 00:00:00.000'
           ,2
           ,13.97
           ,'Wellington Importadora'
           ,'Rua do Mercado, 12'
           ,'Resende'
           ,'SP'
           ,'08737-363'
           ,'Brazil') 
   SET @cnt = @cnt + 1;
END;




SELECT * FROM [dbo].[Orders]



SP_HELPTEXT 'USP_OBTENER_PEDIDOS'

WAITFOR DELAY '00:00:02';


CREATE PROCEDURE USP_OBTENER_PEDIDOS @VCH_ID_CLIENTE VARCHAR (5)      

EXEC USP_OBTENER_PEDIDOS 'WELLI'
GO

