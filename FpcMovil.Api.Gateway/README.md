﻿## Desplegar en IIS

### Instalar el [ASP.NET](http://ASP.NET) Core Hosting Bundle

Este proceso es necesario para que el proyecto pueda ser desplegado en IIS.

Descargar el instaldor en el siguiente enlace: [Instalador](https://dotnet.microsoft.com/en-us/download/dotnet/3.1)

Seleccionar la versión para Windows, [x64](https://dotnet.microsoft.com/en-us/download/dotnet/thank-you/sdk-3.1.420-windows-x64-installer) [x86](https://dotnet.microsoft.com/en-us/download/dotnet/thank-you/sdk-3.1.420-windows-x86-installer) según corresponda.

