using FpcMovil.Api.Gateway.Extensiones;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Ocelot.DependencyInjection;
using Ocelot.Middleware;
using Ocelot.Provider.Consul;
using System.Text;

namespace FpcMovil.Api.Gateway
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            var ocelotConfiguration = new ConfigurationBuilder();
            ocelotConfiguration.AddJsonFile("ocelot.json", optional: false, reloadOnChange: true);
            services.AddOcelot(ocelotConfiguration.Build()).AddConsul();
            services.ConfigurarOpcionesOcelot(Configuration);


            var autorizadorCmact = "Autorizador.Cmact";
            var claveSecreta = Encoding.ASCII.GetBytes("demo");

            services.AddAuthentication(opciones =>
            {
                opciones.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                opciones.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(autorizadorCmact, x =>
            {
                x.SaveToken = false;
                x.RequireHttpsMetadata = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    IssuerSigningKey = new SymmetricSecurityKey(claveSecreta),
                    ValidateIssuer = true,
                    ValidateAudience = true,

                    ValidIssuer = "localhost",
                    ValidAudiences = new string[] { "localhost" }

                    /**
                    ClockSkew = TimeSpan.Zero,
                    ValidateAudience = true,
                    ValidateIssuer = true,
                    ValidateIssuerSigningKey = true,
                    ValidateLifetime = true,
                   
                    // Valicación de audiencias.
                    IssuerSigningKeys = audienciasPermitidas.Select(audiencia =>
                            new SymmetricSecurityKey(Convert.FromBase64String(audiencia.IdSecreto))),
                    ValidIssuers = audienciasPermitidas.Select(audiencia =>
                            audiencia.DireccionOrigenPermitido).Distinct(),
                    ValidAudiences = audienciasPermitidas.Select(audiencia => audiencia.IdAudiencia) **/
                };
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseOcelot().Wait();
        }
    }
}
