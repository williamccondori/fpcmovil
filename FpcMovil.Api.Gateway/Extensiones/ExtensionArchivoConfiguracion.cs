﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Ocelot.Configuration.File;
using System.Collections.Generic;

namespace FpcMovil.Api.Gateway.Extensiones
{
    public static class ExtensionArchivoConfiguracion
    {
        public class GlobalHosts : Dictionary<string, object> { }

        public static IServiceCollection ConfigurarOpcionesOcelot(this IServiceCollection services, IConfiguration configuration)
        {
            services.PostConfigure<FileConfiguration>(fileConfiguration =>
            {
                var consulHost = configuration.GetValue<string>("ServiceDiscovery:Ip", default);
                var consulPuerto = configuration.GetValue<int>("ServiceDiscovery:Puerto", default);
                var consulEsquema = configuration.GetValue<string>("ServiceDiscovery:Esquema", "http");
                var opcionesDescubridor = fileConfiguration.GlobalConfiguration.ServiceDiscoveryProvider;
                opcionesDescubridor.Host = consulHost;
                opcionesDescubridor.Port = consulPuerto;
                opcionesDescubridor.Scheme = consulEsquema;
                opcionesDescubridor.Type = "Consul";
            });

            return services;
        }

    }
}
