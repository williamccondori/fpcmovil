﻿using FpcMovil.Dominio.Repositorios;
using FpcMovil.Infraestructura.EntityFramework.Repositorios;
using Microsoft.Extensions.DependencyInjection;

namespace FpcMovil.Infraestructura.EntityFramework
{
    public static class ExtensionServicio
    {
        public static void AgregarCapaInfraestructuraEntityFramework(this IServiceCollection servicios)
        {
            servicios.AddScoped<IRepositorioOrden, RepositorioOrden>();
        }
    }
}
