﻿using FpcMovil.Dominio.Entidades.ProcedimientosAlmacenados;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace FpcMovil.Infraestructura.EntityFramework.Contextos
{
    public class ContextoSaf : ContextoBase<ContextoSaf>
    {
        public DbSet<ResultadoObtenerPedidos> ResultadoObtenerPedidos { get; set; }

        public ContextoSaf(DbContextOptions<ContextoSaf> opciones, ILogger<ContextoSaf> bitacora) : base(opciones, bitacora)
        {
            Bitacora.LogTrace("Iniciando instancia SQLSERVER (SAF)");
        }
    }
}
