﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FpcMovil.Infraestructura.EntityFramework.Contextos
{
    public class ContextoBase<T> : DbContext where T : DbContext
    {
        protected readonly ILogger<T> Bitacora;

        protected ContextoBase(DbContextOptions<T> opciones, ILogger<T> bitacora) : base(opciones)
        {
            Bitacora = bitacora;
            Bitacora.LogTrace("Iniciando instancia SQLSERVER (BASE)");
        }

        // https://www.codeproject.com/Articles/832189/List-vs-IEnumerable-vs-IQueryable-vs-ICollection-v
        // https://github.com/Tillman32/CleanArchitecture/blob/master/src/CleanArchitecture.Core/Service/ArticleService.cs
        public IEnumerable<TRespuesta> EjecutarProcedimientoAlmacenado<TRespuesta>(string nombreProcedimientoAlmacenado,
            SqlParameter[] parametros) where TRespuesta : class
        {
            DateTime tiempoInicio = DateTime.Now;
            try
            {
                Bitacora.LogTrace("Inicia procedimiento almacenado: {NombreProcedimientoAlmacenado}",
                    nombreProcedimientoAlmacenado);
                var cadenaParametros = string.Join(",", parametros.Select(x => $"{x.ParameterName}"));
                if (!string.IsNullOrEmpty(cadenaParametros))
                    nombreProcedimientoAlmacenado = $"{nombreProcedimientoAlmacenado} {cadenaParametros}";
                Bitacora.LogTrace("Ejecutando procedimiento almacenado: {NombreProcedimientoAlmacenado}",
                    nombreProcedimientoAlmacenado);
                return Set<TRespuesta>()
                    .FromSqlRaw($"EXEC {nombreProcedimientoAlmacenado}", parametros.ToArray<object>())
                    .AsNoTracking()
                    .ToList();
            }
            catch (Exception excepcion)
            {
                Bitacora.LogError(excepcion,
                    "Error al ejecutar procedimiento almacenado: {NombreProcedimientoAlmacenado}",
                    nombreProcedimientoAlmacenado);
                throw new Exception("Error al consultar el procedimiento almacenado");
            }
            finally
            {
                var segundos = (DateTime.Now - tiempoInicio).Seconds;
                Bitacora.LogTrace("Termina procedimiento almacenado: {NombreProcedimientoAlmacenado} en {Segundos} segundos", nombreProcedimientoAlmacenado, segundos);
            }
        }
    }
}