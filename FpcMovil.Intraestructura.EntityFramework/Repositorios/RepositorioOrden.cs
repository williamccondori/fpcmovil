﻿using FpcMovil.Dominio.Entidades;
using FpcMovil.Dominio.Entidades.ProcedimientosAlmacenados;
using FpcMovil.Dominio.Repositorios;
using FpcMovil.Infraestructura.EntityFramework.Contextos;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FpcMovil.Infraestructura.EntityFramework.Repositorios
{
    public class RepositorioOrden : IRepositorioOrden, IDisposable
    {

        private readonly ContextoSaf _contexto;

        public RepositorioOrden(ContextoSaf contexto)
        {
            _contexto = contexto;
        }

        public IEnumerable<Orden> ObtenerTodosPedidos(string idCliente)
        {
            var parametros = new[]
            {
                new SqlParameter("@VCH_ID_CLIENTE", idCliente)
            };

            return _contexto.EjecutarProcedimientoAlmacenado<ResultadoObtenerPedidos>
                ("USP_OBTENER_PEDIDOS", parametros).Select(x => new Orden
                {
                    Id = x.ID,
                    FechaEnvio = x.FECHA_ENVIO,
                    FechaPedido = x.FECHA_PEDIDO,
                    FechaTransaccion = x.FECHA_TRANSACCION
                });
        }

        public void Dispose()
        {
            if (_contexto != null)
            {
                _contexto.Dispose();
            }
        }
    }
}
