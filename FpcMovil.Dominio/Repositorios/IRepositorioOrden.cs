﻿using FpcMovil.Dominio.Entidades;
using System.Collections.Generic;

namespace FpcMovil.Dominio.Repositorios
{
    public interface IRepositorioOrden
    {
        IEnumerable<Orden> ObtenerTodosPedidos(string idCliente);
    }
}
