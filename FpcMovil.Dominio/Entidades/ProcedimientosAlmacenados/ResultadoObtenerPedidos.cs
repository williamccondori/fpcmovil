﻿using System;

namespace FpcMovil.Dominio.Entidades.ProcedimientosAlmacenados
{
    public class ResultadoObtenerPedidos
    {
        public int ID { get; set; }
        public DateTime FECHA_PEDIDO { get; set; }
        public DateTime FECHA_TRANSACCION { get; set; }
        public DateTime FECHA_ENVIO { get; set; }
    }
}
