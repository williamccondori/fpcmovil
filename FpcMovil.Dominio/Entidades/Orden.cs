﻿using System;

namespace FpcMovil.Dominio.Entidades
{
    public class Orden
    {
        public int Id { get; set; }
        public DateTime FechaPedido { get; set; }
        public DateTime FechaTransaccion { get; set; }
        public DateTime FechaEnvio { get; set; }
    }
}
