﻿using FpcMovil.Dominio.Repositorios;
using FpcMovil.Infraestructura.EntityFramework.Repositorios;
using Microsoft.Extensions.DependencyInjection;

namespace FpcMovil.Infraestructura
{
    public static class ServicioExtension
    {
        public static void AgregarCapaInfraestructura(this IServiceCollection servicios)
        {
            servicios.AddScoped<IRepositorioOrden, RepositorioOrden>();
        }
    }
}
